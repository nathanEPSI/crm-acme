# CRM ACME

CRM ACME est une application web ayant pour objectif de faciliter la gestion des relations client des employés commerciaux d'ACME.
Depuis CRM ACME il est possible de consulter la base de clients existante, d'ajouter, de supprimer et de modifier des fiches clients.

## Technologies

L'application est lancée sur navigateur et est utilisable sur plusieurs supports (desktop, tablettes, mobiles).

### Front-end

Le front-end est réalisé avec le framework Angular et est responsive (à minima support desktop et mobile).

### Back-end

Back-end en java (framework à déterminer).

### Base de données

A déterminer.

## Scénario d'utilisation

1. Page de connexion/création de compte
2. Dashboard avec accès aux différents onglets
    1. Bouton de déconnexion qui renvoie à la page de connexion/création de compte
    2. Onglet client
    3. Onglet ?
    4. Onglet ?

## Installation

## Utilisation