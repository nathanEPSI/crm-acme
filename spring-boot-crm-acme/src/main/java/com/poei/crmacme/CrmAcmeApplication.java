package com.poei.crmacme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrmAcmeApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrmAcmeApplication.class, args);
	}

}
