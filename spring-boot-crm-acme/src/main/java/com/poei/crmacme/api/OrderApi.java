package com.poei.crmacme.api;

import com.poei.crmacme.api.dto.OrderDto;
import com.poei.crmacme.exception.UnknownResourceException;
import com.poei.crmacme.mapper.OrderMapper;
import com.poei.crmacme.model.Order;
import com.poei.crmacme.service.CustomerService;
import com.poei.crmacme.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/orders")
public class OrderApi {

    private final OrderService orderService;
    private final CustomerService customerService;

    private final OrderMapper orderMapper;


    public OrderApi(OrderService orderService, OrderMapper orderMapper, CustomerService customerService) {
        this.orderService = orderService;
        this.orderMapper = orderMapper;
        this.customerService = customerService;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Get all orders")
    public ResponseEntity<List<OrderDto>> getAll() {
        return ResponseEntity.ok(orderService.getAll().stream().map(orderMapper::mapToDto).toList());
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Get order by ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "return found order"),
            @ApiResponse(responseCode = "404", description = "no order found for given ID")
    })
    public ResponseEntity<OrderDto> getById(@PathVariable final Long id) {
        try {
            return ResponseEntity.ok(orderMapper.mapToDto(orderService.getById(id)));
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ure.getMessage());
        }
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Create a new order")
    @ApiResponses({
            @ApiResponse(responseCode = "201", description = "Order successfully created"),
            @ApiResponse(responseCode = "404", description = "No customer associated with this order")
    })
    public ResponseEntity<OrderDto> create(@RequestBody final OrderDto orderDto) {
        Order order = orderMapper.mapToModel(orderDto);
        try{
            order.setCustomer(customerService.getById(orderDto.getCustomerId()));
            OrderDto orderDtoResponse = orderMapper.mapToDto(
                    orderService.create(order)
            );
            return ResponseEntity.created(URI.create("/v1/orders/" + orderDtoResponse.getId())).body(orderDtoResponse);
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ure.getMessage());
        }
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Update an existing order")
    @ApiResponses({@ApiResponse(responseCode = "204", description = "No content")})
    public ResponseEntity<Void> update(
            @PathVariable final Long id,
            @RequestBody OrderDto orderDto
    ) {
        try {
            orderDto.setId(id);
            orderService.update(orderMapper.mapToModel(orderDto));
            return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ure.getMessage());
        }
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete an order for the given ID")
    @ApiResponses({@ApiResponse(responseCode = "204", description = "No content"), @ApiResponse(responseCode = "404", description = "No order found for given id")})
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        try {
            orderService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ure.getMessage());
        }
    }
}
