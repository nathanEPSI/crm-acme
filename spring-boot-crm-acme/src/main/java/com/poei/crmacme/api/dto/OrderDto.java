package com.poei.crmacme.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {

    private Long id;

    private Long customerId;

    private BigDecimal totalExclTax;

    private Date ordersDate;

    private Boolean deliveryStatus;

    private Boolean paymentStatus;
}
