package com.poei.crmacme.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDto {

    private Long id;

    private String firstname;

    private String lastname;

    private String company;

    private String email;

    private String phone;

    private Boolean active;

    private List<OrderDto> orders;
}
