package com.poei.crmacme.api;

import com.poei.crmacme.api.dto.ProductDto;
import com.poei.crmacme.exception.UnknownResourceException;
import com.poei.crmacme.mapper.ProductMapper;
import com.poei.crmacme.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/products")
public class ProductApi {

    private final ProductService productService;
    private final ProductMapper productMapper;

    public ProductApi(ProductService productService, ProductMapper productMapper) {
        this.productService = productService;
        this.productMapper = productMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Get all products")
    public ResponseEntity<List<ProductDto>> getAll(){
        return ResponseEntity.ok(this.productService.getAll().stream().map(productMapper::mapToDto).toList());
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Get product by ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "return found product"),
            @ApiResponse(responseCode = "404", description = "no order found for given product ID")
    })
    public ResponseEntity<ProductDto> getById(@PathVariable final Long id){
        try {
            return ResponseEntity.ok(this.productMapper.mapToDto(this.productService.getById(id)));
        } catch (UnknownResourceException ure){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ure.getMessage());
        }
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Create a new product")
    @ApiResponses({
            @ApiResponse(responseCode = "201", description = "Product successfully created"),
            @ApiResponse(responseCode = "404", description = "No product associated with this product")
    })
    public ResponseEntity<ProductDto> create(@RequestBody final ProductDto productDto){
        ProductDto productDtoResponse = this.productMapper.mapToDto(this.productService.create(this.productMapper.mapToModel(productDto)));
        return ResponseEntity.created(URI.create("/v1/products" + productDtoResponse.getId())).body(productDtoResponse);
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Update an existing product")
    @ApiResponses({
            @ApiResponse(responseCode = "204" , description = "No content")
    })
    public ResponseEntity<Void> update(
            @PathVariable final Long id,
            @RequestBody ProductDto productDto
    ) {
        try {
            productDto.setId(id);
            this.productService.update(this.productMapper.mapToModel(productDto));
            return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ure.getMessage());
        }
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete a product for the given ID")
    @ApiResponses({
            @ApiResponse(responseCode = "204" , description = "No content")
    })
    public ResponseEntity<Void> deleteById(@PathVariable final Long id){
        try {
           this.productService.delete(id);
           return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ure.getMessage());
        }
    }
}

