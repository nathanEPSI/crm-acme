package com.poei.crmacme.api;

import com.poei.crmacme.api.dto.UserDto;
import com.poei.crmacme.exception.UnknownResourceException;
import com.poei.crmacme.mapper.UserMapper;
import com.poei.crmacme.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/users")
public class UserApi {

    private final UserService userService;

    private final UserMapper userMapper;

    public UserApi(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Get all users")
    public ResponseEntity<List<UserDto>> getAll() {
        return ResponseEntity.ok(userService.getAll().stream().map(userMapper::mapToDto).toList());
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Get user by ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Return found user"),
            @ApiResponse(responseCode = "404", description = "No user found for given ID")
    })
    public ResponseEntity<UserDto> getById(@PathVariable final Long id) {
        try {
            return ResponseEntity.ok(userMapper.mapToDto(userService.getById(id)));
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ure.getMessage());
        }
    }

    @GetMapping(path = "/login", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Return a user by its username and password")
    public ResponseEntity<UserDto> login(@RequestParam String username, @RequestParam String password) {
        try {
            return ResponseEntity.ok(
                    userMapper.mapToDto(userService.getByUsernameAndPassword(username, password))
            );
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ure.getMessage());
        }
    }


    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Create a new user")
    @ApiResponse(responseCode = "201", description = "User successfully created")
    public ResponseEntity<UserDto> create(@RequestBody final UserDto userDto) {
        UserDto userDtoResponse = this.userMapper.mapToDto(
                this.userService.create(this.userMapper.mapToModel(userDto)));
        return ResponseEntity.created(URI.create("/v1/users/" + userDtoResponse.getId())).body(userDtoResponse);
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Update an existing user")
    @ApiResponses({@ApiResponse(responseCode = "204", description = "No content")})
    public ResponseEntity<Void> update(
            @PathVariable final Long id,
            @RequestBody UserDto userDto
    ) {
        try {
            userDto.setId(id);
            userService.update(userMapper.mapToModel(userDto));
            return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ure.getMessage());
        }
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete a user for the given ID")
    @ApiResponses({@ApiResponse(responseCode = "204", description = "No content"), @ApiResponse(responseCode = "404", description = "No user found for given ID")})
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        try {
            this.userService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ure.getMessage());
        }
    }
}
