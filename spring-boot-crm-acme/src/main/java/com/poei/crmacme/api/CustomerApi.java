package com.poei.crmacme.api;

import com.poei.crmacme.api.dto.CustomerDto;
import com.poei.crmacme.exception.UnknownResourceException;
import com.poei.crmacme.mapper.CustomerMapper;
import com.poei.crmacme.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/customers")
public class CustomerApi {
    private final CustomerService customerService;

    private final CustomerMapper customerMapper;

    public CustomerApi(CustomerService customerService, CustomerMapper customerMapper) {
        this.customerService = customerService;
        this.customerMapper = customerMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Get all customers")
    public ResponseEntity<List<CustomerDto>> getAll() {
        return ResponseEntity.ok(
                this.customerService.getAll()
                        .stream()
                        .map(this.customerMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get customer by ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "return found customer"),
            @ApiResponse(responseCode = "404", description = "no customer found for given ID")
    })
    public ResponseEntity<CustomerDto> getById(@PathVariable final Long id) {
        try {
            return ResponseEntity.ok(this.customerMapper.mapToDto(this.customerService.getById(id)));
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ure.getMessage());
        }
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Create a new customer")
    @ApiResponse(responseCode = "201", description = "Customer successfully created")
    public ResponseEntity<CustomerDto> create(@RequestBody final CustomerDto customerDto) {
        CustomerDto customerDtoResponse = this.customerMapper.mapToDto(
                this.customerService.create(this.customerMapper.mapToModel(customerDto)));
        return ResponseEntity.created(URI.create("/v1/customers/" + customerDtoResponse.getId())).body(customerDtoResponse);
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Update an existing customer")
    @ApiResponses({@ApiResponse(responseCode = "204", description = "No content")})
    public ResponseEntity<Void> update(
            @PathVariable final Long id,
            @RequestBody CustomerDto customerDto
    ) {
        try {
            customerDto.setId(id);
            this.customerService.update(this.customerMapper.mapToModel(customerDto));
            return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ure.getMessage());
        }
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete a customer for the given ID")
    @ApiResponses({@ApiResponse(responseCode = "204", description = "No content"), @ApiResponse(responseCode = "404", description = "no customer found for given id")})
    public ResponseEntity<Void> deleteById(@PathVariable final Long id) {
        try {
            this.customerService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ure.getMessage());
        }
    }
}
