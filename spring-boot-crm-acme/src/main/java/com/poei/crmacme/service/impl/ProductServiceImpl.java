package com.poei.crmacme.service.impl;

import com.poei.crmacme.exception.UnknownResourceException;
import com.poei.crmacme.model.Product;
import com.poei.crmacme.repository.ProductRepository;
import com.poei.crmacme.service.ProductService;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Primary
public class ProductServiceImpl implements ProductService{

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    @Override
    public List<Product> getAll() {
        return this.productRepository.findAll(Sort.by("name").ascending());
    }

    @Override
    public Product getById(Long id) throws UnknownResourceException{
        Optional<Product> product = this.productRepository.findById(id);
        return product.orElseThrow(UnknownResourceException::new);
    }

    @Override
    public Product create(Product product) {
        return this.productRepository.save(product);
    }

    @Override
    public Product update(Product product) {
        Product productToUpdate = this.getById(product.getId());
        productToUpdate.setName(product.getName());
        productToUpdate.setPriceExclTax(product.getPriceExclTax());
        productToUpdate.setTaxRate(product.getTaxRate());
        productToUpdate.setOrders(product.getOrders());
        return this.productRepository.save(productToUpdate);
    }

    @Override
    public void delete(Long id) {
        Product productToDelete = this.getById(id);
        this.productRepository.delete(productToDelete);
    }
}
