package com.poei.crmacme.service.impl;

import com.poei.crmacme.exception.UnknownResourceException;
import com.poei.crmacme.model.Customer;
import com.poei.crmacme.repository.CustomerRepository;
import com.poei.crmacme.service.CustomerService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Primary
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAll() {
        return this.customerRepository.findAll();
    }

    @Override
    public Customer getById(Long id) throws UnknownResourceException {
        Optional<Customer> customer = this.customerRepository.findById(id);
        return customer.orElseThrow(UnknownResourceException::new);
    }

    @Override
    public Customer create(Customer customer) {
        return this.customerRepository.save(customer);
    }

    @Override
    public Customer update(Customer customer) {
        Customer customerToUpdate = this.getById(customer.getId());
        customerToUpdate.setFirstname(customer.getFirstname());
        customerToUpdate.setLastname(customer.getLastname());
        customerToUpdate.setCompany(customer.getCompany());
        customerToUpdate.setEmail(customer.getEmail());
        customerToUpdate.setPhone(customer.getPhone());
        customerToUpdate.setActive(customer.getActive());
        customerToUpdate.setOrders(customer.getOrders());
        return this.customerRepository.save(customerToUpdate);
    }

    @Override
    public void delete(Long id) {
        Customer customerToDelete = this.getById(id);
        this.customerRepository.delete(customerToDelete);
    }
}
