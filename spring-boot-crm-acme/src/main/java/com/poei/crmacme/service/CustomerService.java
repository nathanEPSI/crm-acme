package com.poei.crmacme.service;

import com.poei.crmacme.model.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getAll();

    Customer getById(Long id);

    Customer create(Customer customer);

    Customer update(Customer customer);

    void delete(Long id);
}
