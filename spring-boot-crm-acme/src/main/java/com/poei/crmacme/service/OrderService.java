package com.poei.crmacme.service;

import com.poei.crmacme.model.Order;

import java.util.List;

public interface OrderService {

    List<Order> getAll();

    Order getById(Long id);

    Order create(Order order);

    Order update(Order order);

    void delete(Long id);
}
