package com.poei.crmacme.service.impl;

import com.poei.crmacme.exception.UnknownResourceException;
import com.poei.crmacme.model.Order;
import com.poei.crmacme.repository.OrderRepository;
import com.poei.crmacme.service.OrderService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }


    @Override
    public List<Order> getAll() {
        return orderRepository.findAll();
    }

    @Override
    public Order getById(Long id) throws UnknownResourceException {
        return orderRepository.findById(id).orElseThrow(UnknownResourceException::new);
    }

    @Override
    public Order create(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public Order update(Order order) {
        Order orderToUpdate = this.getById(order.getId());
        orderToUpdate.setOrdersDate(order.getOrdersDate());
        orderToUpdate.setTotalExclTax(order.getTotalExclTax());
        orderToUpdate.setDeliveryStatus(order.getDeliveryStatus());
        orderToUpdate.setPaymentStatus(order.getPaymentStatus());
        orderToUpdate.setCustomer(order.getCustomer());
        orderToUpdate.setProducts(order.getProducts());
        return orderRepository.save(orderToUpdate);
    }

    @Override
    public void delete(Long id) {
        Order orderToDelete = this.getById(id);
        orderRepository.delete(orderToDelete);
    }
}
