package com.poei.crmacme.service.impl;

import com.poei.crmacme.exception.UnknownResourceException;
import com.poei.crmacme.model.User;
import com.poei.crmacme.repository.UserRepository;
import com.poei.crmacme.service.UserService;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Example;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Primary
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User getById(Long id) throws UnknownResourceException {
        return userRepository.findById(id).orElseThrow(UnknownResourceException::new);
    }

    @Override
    public User getByUsernameAndPassword(String username, String password) {
        User user = this.userRepository.findByUsername(username).get();
        if (new BCryptPasswordEncoder().matches(password, user.getPassword())) {
            return user;
        }
        throw new UnknownResourceException("No user found for the given user/password");

    }

    @Override
    public User create(User user) {
        user.setId(null);
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public User update(User user) {
        User userToUpdate = this.getById(user.getId());
        userToUpdate.setUsername(user.getUsername());
        userToUpdate.setPassword(user.getPassword());
        userToUpdate.setEmail(user.getEmail());
        userToUpdate.setType(user.getType());
        return userRepository.save(userToUpdate);
    }

    @Override
    public void delete(Long id) {
        User userToDelete = this.getById(id);
        userRepository.delete(userToDelete);
    }

    @Override
    public User getByUsername(String username) {
        return this.userRepository.findByUsername(username)
                .orElseThrow(() -> new UnknownResourceException("No user found for this username."));
    }

}
