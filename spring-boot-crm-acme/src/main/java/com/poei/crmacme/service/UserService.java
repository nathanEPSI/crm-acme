package com.poei.crmacme.service;

import com.poei.crmacme.model.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getById(Long id);

    User getByUsernameAndPassword(String username, String password);

    User create(User user);

    User update(User user);

    void delete(Long id);

    User getByUsername(String username);


}
