package com.poei.crmacme.service;

import com.poei.crmacme.model.Product;

import java.util.List;

public interface ProductService {

    List<Product> getAll();

    Product getById(Long id);

    Product create(Product product);

    Product update(Product product);

    void delete(Long id);
}
