package com.poei.crmacme.mapper;

import com.poei.crmacme.api.dto.OrderDto;
import com.poei.crmacme.model.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface OrderMapper {

    @Mapping(source = "customer.id", target = "customerId")
    OrderDto mapToDto(Order order);

    @Mapping(source = "customerId", target = "customer.id")
    Order mapToModel(OrderDto orderDto);
}
