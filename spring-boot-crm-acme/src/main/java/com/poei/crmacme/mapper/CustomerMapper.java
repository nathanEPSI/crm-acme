package com.poei.crmacme.mapper;

import com.poei.crmacme.api.dto.CustomerDto;
import com.poei.crmacme.api.dto.OrderDto;
import com.poei.crmacme.model.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueMappingStrategy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface CustomerMapper {

    @Mapping(target = "orders", expression = "java(getOrders(customer))")
    CustomerDto mapToDto(Customer customer);

    default List<OrderDto> getOrders(Customer customer) {
        List<OrderDto> orders = new ArrayList<>();
        if (customer.getOrders() != null) {
            orders = customer.getOrders().stream()
                    .map(order -> new OrderDto(
                            order.getId(),
                            customer.getId(),
                            order.getTotalExclTax(),
                            order.getOrdersDate(),
                            order.getDeliveryStatus(),
                            order.getPaymentStatus()
                    ))
                    .toList();
        }
        return orders;
    }

    Customer mapToModel(CustomerDto customerDto);

}
