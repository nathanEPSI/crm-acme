package com.poei.crmacme.mapper;

import com.poei.crmacme.api.dto.UserDto;
import com.poei.crmacme.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface UserMapper {

    @Mapping(source = "type.id", target = "type")
    UserDto mapToDto(User user);

    @Mapping(source = "type", target = "type.id")
    User mapToModel(UserDto userDto);

}
