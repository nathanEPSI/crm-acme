package com.poei.crmacme.mapper;

import com.poei.crmacme.api.dto.ProductDto;
import com.poei.crmacme.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;
import org.springframework.stereotype.Component;


@Component
@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface ProductMapper {

    ProductDto mapToDto(Product product);

    Product mapToModel(ProductDto productDto);
}
