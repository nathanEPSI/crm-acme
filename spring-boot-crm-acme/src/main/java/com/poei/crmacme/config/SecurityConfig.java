package com.poei.crmacme.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public UserDetailsService userDetailsService() {
        return new MyUserDetailsServiceImpl();
    }


    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setPasswordEncoder(passwordEncoder());
        authProvider.setUserDetailsService(userDetailsService());
        return authProvider;
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) {
        authenticationManagerBuilder.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and().authorizeRequests()
                .antMatchers(HttpMethod.GET, "/v1/customers/**").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.POST, "/v1/customers/**").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.PUT, "/v1/customers/**").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.DELETE, "/v1/customers/**").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.GET, "/v1/orders/**").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.POST, "/v1/orders/**").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.PUT, "/v1/orders/**").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.DELETE, "/v1/orders/**").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.GET, "/v1/products/**").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.POST, "/v1/products/**").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.PUT, "/v1/products/**").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.DELETE, "/v1/products/**").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.PUT, "/v1/users/**").hasAnyRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/v1/users/**").hasAnyRole("ADMIN")
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/v1/logout"))
                .logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler(HttpStatus.OK))
                .deleteCookies("JSESSIONID")
                .invalidateHttpSession(true)
                .and().csrf().disable();

    }

}
