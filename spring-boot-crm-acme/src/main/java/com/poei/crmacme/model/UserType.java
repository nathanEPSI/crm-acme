package com.poei.crmacme.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="dict_user_type")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "TINYINT")
    private Short id;

    @Column(name = "user_type")
    private String userType;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "type")
    private Set<User> users;
}
