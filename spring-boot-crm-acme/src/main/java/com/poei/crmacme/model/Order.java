package com.poei.crmacme.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="orders")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(cascade = { CascadeType.PERSIST })
    @JoinTable(
            name = "orders_products",
            joinColumns = { @JoinColumn(name = "orders_id") },
            inverseJoinColumns = { @JoinColumn(name = "products_id") }
    )
    Set<Product> products = new HashSet<>();

    @ManyToOne
    @JoinColumn(name="customer_id", nullable = false)
    private Customer customer;

    @Column(name = "total_excl_tax")
    private BigDecimal totalExclTax;

    @CreationTimestamp
    @Column(name = "orders_date")
    private Date ordersDate;

    @Column(name = "delivery_status")
    private Boolean deliveryStatus;

    @Column(name = "payment_status")
    private Boolean paymentStatus;
}
