package com.poei.crmacme.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "products")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(mappedBy = "products")
    private Set<Order> orders = new HashSet<>();

    private String name;

    @Column(name = "price_excl_tax")
    private BigDecimal priceExclTax;

    @Column(name = "tax_rate")
    private BigDecimal taxRate;
}
