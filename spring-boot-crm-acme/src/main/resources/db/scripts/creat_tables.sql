create table customers
(
    id        bigint auto_increment
        primary key,
    firstname varchar(255) null,
    lastname  varchar(255) null,
    company   varchar(255) null,
    email     varchar(255) null,
    phone     varchar(255) null,
    active    tinyint(1)   null
);

create table dict_user_type
(
    id        tinyint auto_increment
        primary key,
    user_type varchar(255) null
);

create table orders
(
    id              bigint auto_increment
        primary key,
    customer_id     bigint     null,
    total_excl_tax  float      null,
    orders_date     date       null,
    delivery_status tinyint(1) null,
    payment_status  tinyint(1) null,
    constraint orders_customer_id_fk
        foreign key (customer_id) references customers (id)
);

create table products
(
    id             bigint auto_increment
        primary key,
    name           varchar(255) null,
    price_excl_tax decimal      null,
    tax_rate       decimal      null
);

create table orders_products
(
    orders_id        bigint not null,
    products_id      bigint not null,
    product_quantity int    null,
    primary key (orders_id, products_id),
    constraint orders_product_orders_id_fk
        foreign key (orders_id) references orders (id),
    constraint orders_product_product_id_fk
        foreign key (products_id) references products (id)
);

create table users
(
    id           bigint auto_increment
        primary key,
    username     varchar(255) null,
    password     varchar(255) null,
    email        varchar(255) null,
    user_type_id tinyint      not null,
    constraint user_type_id_fk
        foreign key (user_type_id) references dict_user_type (id)
);

