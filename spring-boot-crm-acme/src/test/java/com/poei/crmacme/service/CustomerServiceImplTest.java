package com.poei.crmacme.service;


import com.poei.crmacme.model.Customer;
import com.poei.crmacme.repository.CustomerRepository;
import com.poei.crmacme.service.impl.CustomerServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class CustomerServiceImplTest {

    @InjectMocks
    private CustomerServiceImpl customerService;

    @Mock
    private CustomerRepository customerRepository;

    @Test
    void getAll() {
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer(1L, new HashSet<>(), "test", "test", "company", "email", "phone", true));
        Mockito.when(customerRepository.findAll()).thenReturn(customers);

        List<Customer> customersResult = customerService.getAll();
        assertEquals(1, customersResult.size());
    }

    @Test
    void getById() {
        Optional<Customer> customerTest = Optional.of(new Customer(1L, new HashSet<>(), "test", "test", "company", "email", "phone", true));
        Mockito.when(customerRepository.findById(1L)).thenReturn(customerTest);

        Customer customer = this.customerService.getById(1L);
        assertNotNull(customer);
        assertEquals(1L, customer.getId());
    }

    @Test
    void create() {
        Customer customerTest = new Customer(1L, new HashSet<>(), "test", "test", "company", "email", "phone", true);
        Mockito.when(customerRepository.save(customerTest)).thenReturn(customerTest);

        Customer customer = this.customerService.create(customerTest);
        assertNotNull(customer);
        assertEquals(1L, customer.getId());
    }

    @Test
    void update() {
        Customer customer = new Customer();
        customer.setId(1L);
        customer.setFirstname("Bob");
        Optional<Customer> customerTest = Optional.of(customer);

        Customer modifiedCustomer = new Customer();
        modifiedCustomer.setId(1L);
        modifiedCustomer.setFirstname("Joe");

        Mockito.when(customerRepository.findById(1L)).thenReturn(customerTest);
        Mockito.when(customerRepository.save(customer)).thenReturn(modifiedCustomer);
        try {
            customerService.update(modifiedCustomer);
        } catch (Exception e){
            fail();
        }
        Mockito.verify(customerRepository, Mockito.times(1)).save(customer);
    }

    @Test
    void delete() {
        Customer customer = new Customer();
        customer.setId(3L);
        Optional<Customer> customerTest = Optional.of(customer);

        Mockito.when(customerRepository.findById(3L)).thenReturn(customerTest);
        Mockito.doNothing().when(customerRepository).delete(customer);

        try {
            customerService.delete(3L);
        } catch (Exception e){
            fail();
        }
        Mockito.verify(customerRepository, Mockito.times(1)).delete(customer);
    }
}