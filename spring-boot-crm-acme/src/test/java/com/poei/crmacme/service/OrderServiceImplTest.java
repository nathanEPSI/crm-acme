package com.poei.crmacme.service;

import com.poei.crmacme.model.Order;
import com.poei.crmacme.repository.OrderRepository;
import com.poei.crmacme.service.impl.OrderServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest {

    @InjectMocks
    private OrderServiceImpl orderService;

    @Mock
    private OrderRepository orderRepository;

    @Test
    void getAll() {
        List<Order> orders = new ArrayList<>();
        orders.add(new Order());
        Mockito.when(orderRepository.findAll()).thenReturn(orders);

        List<Order> ordersResult = this.orderService.getAll();
        assertEquals(1, ordersResult.size());
    }

    @Test
    void getById() {
        Optional<Order> orderTest = Optional.of(new Order());
        orderTest.get().setId(1L);
        Mockito.when(orderRepository.findById(1L)).thenReturn(orderTest);

        Order order = this.orderService.getById(1L);
        assertNotNull(order);
        assertEquals(1L, order.getId());
    }

    @Test
    void create() {
        Order orderTest = new Order();
        orderTest.setId(1L);
        Mockito.when(orderRepository.save(orderTest)).thenReturn(orderTest);

        Order order = this.orderService.create(orderTest);
        assertNotNull(order);
        assertEquals(1L, order.getId());
    }

    @Test
    void update() {
        Order order = new Order();
        order.setId(1L);
        order.setTotalExclTax(BigDecimal.valueOf(10.0));
        Optional<Order> orderTest = Optional.of(order);

        Order modifiedOrder = new Order();
        modifiedOrder.setId(1L);
        modifiedOrder.setTotalExclTax(BigDecimal.valueOf(12.0));

        Mockito.when(orderRepository.findById(1L)).thenReturn(orderTest);
        Mockito.when(orderRepository.save(order)).thenReturn(modifiedOrder);
        try {
            orderService.update(modifiedOrder);
        } catch (Exception e){
            fail();
        }
        Mockito.verify(orderRepository, Mockito.times(1)).save(order);
    }

    @Test
    void delete() {
        Order order = new Order();
        order.setId(3L);
        Optional<Order> orderTest = Optional.of(order);

        Mockito.when(orderRepository.findById(3L)).thenReturn(orderTest);
        Mockito.doNothing().when(orderRepository).delete(order);

        try {
            orderService.delete(3L);
        } catch (Exception e){
            fail();
        }
        Mockito.verify(orderRepository, Mockito.times(1)).delete(order);
    }
}