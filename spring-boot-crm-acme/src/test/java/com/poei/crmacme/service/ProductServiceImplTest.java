package com.poei.crmacme.service;

import com.poei.crmacme.model.Order;
import com.poei.crmacme.model.Product;
import com.poei.crmacme.repository.ProductRepository;
import com.poei.crmacme.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {

    @InjectMocks
    private ProductServiceImpl productService;

    @Mock
    private ProductRepository productRepository;

    @Test
    void getAll() {
        List<Product> products = new ArrayList<>();
        products.add(new Product());
        Mockito.when(productRepository.findAll(Sort.by("name").ascending())).thenReturn(products);

        List<Product> productsResult = this.productService.getAll();
        assertEquals(1, productsResult.size());
    }

    @Test
    void getById() {
        Optional<Product> productTest = Optional.of(new Product());
        productTest.get().setId(1L);
        Mockito.when(productRepository.findById(1L)).thenReturn(productTest);

        Product product = this.productService.getById(1L);
        assertNotNull(product);
        assertEquals(1L, product.getId());
    }

    @Test
    void create() {
        Product productTest = new Product();
        productTest.setId(1L);
        Mockito.when(productRepository.save(productTest)).thenReturn(productTest);

        Product product = this.productService.create(productTest);
        assertNotNull(product);
        assertEquals(1L, product.getId());
    }

    @Test
    void update() {
        Product product = new Product();
        product.setId(1L);
        product.setName("Dynamite");
        Optional<Product> productTest = Optional.of(product);

        Product modifiedProduct = new Product();
        modifiedProduct.setId(1L);
        modifiedProduct.setName("Poudre Noire");

        Mockito.when(productRepository.findById(1L)).thenReturn(productTest);
        Mockito.when(productRepository.save(product)).thenReturn(modifiedProduct);
        try {
            productService.update(modifiedProduct);
        } catch (Exception e){
            fail();
        }
        Mockito.verify(productRepository, Mockito.times(1)).save(product);
    }

    @Test
    void delete() {
        Product product = new Product();
        product.setId(3L);
        Optional<Product> productTest = Optional.of(product);

        Mockito.when(productRepository.findById(3L)).thenReturn(productTest);
        Mockito.doNothing().when(productRepository).delete(product);

        try {
            productService.delete(3L);
        } catch (Exception e){
            fail();
        }
        Mockito.verify(productRepository, Mockito.times(1)).delete(product);
    }
}