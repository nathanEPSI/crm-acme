package com.poei.crmacme.service;

import com.poei.crmacme.model.Order;
import com.poei.crmacme.model.User;
import com.poei.crmacme.repository.UserRepository;
import com.poei.crmacme.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Test
    void getAll() {
        List<User> users = new ArrayList<>();
        users.add(new User());
        Mockito.when(userRepository.findAll()).thenReturn(users);

        List<User> usersResult = this.userService.getAll();
        assertEquals(1, usersResult.size());
    }

    @Test
    void getById() {
        Optional<User> userTest = Optional.of(new User());
        userTest.get().setId(1L);
        Mockito.when(userRepository.findById(1L)).thenReturn(userTest);

        User user = this.userService.getById(1L);
        assertNotNull(user);
        assertEquals(1L, user.getId());
    }

    @Test
    void getByUsernameAndPassword() {
    }

    @Test
    void create() {
        User userTest = new User();
        userTest.setId(1L);
        Mockito.when(userRepository.save(userTest)).thenReturn(userTest);

        User user = this.userService.create(userTest);
        assertNotNull(user);
        assertEquals(1L, user.getId());
    }

    @Test
    void update() {
        User user = new User();
        user.setId(1L);
        user.setUsername("toto");
        Optional<User> userTest = Optional.of(user);

        User modifiedUser = new User();
        modifiedUser.setId(1L);
        modifiedUser.setUsername("tutu");

        Mockito.when(userRepository.findById(1L)).thenReturn(userTest);
        Mockito.when(userRepository.save(user)).thenReturn(modifiedUser);
        try {
            userService.update(modifiedUser);
        } catch (Exception e){
            fail();
        }
        Mockito.verify(userRepository, Mockito.times(1)).save(user);
    }

    @Test
    void delete() {
        User user = new User();
        user.setId(3L);
        Optional<User> userTest = Optional.of(user);

        Mockito.when(userRepository.findById(3L)).thenReturn(userTest);
        Mockito.doNothing().when(userRepository).delete(user);

        try {
            userService.delete(3L);
        } catch (Exception e){
            fail();
        }
        Mockito.verify(userRepository, Mockito.times(1)).delete(user);
    }

    @Test
    void getByUsername() {
    }
}