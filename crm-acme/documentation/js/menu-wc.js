'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">crm-acme documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-62bf2c07dffb82cead56215221735e586d5fd9b5dab0caf8283b91e214e8ce797de3772954db8ea14e6a894a0e26f4272e0792d03001fd3bf71f91a9b4edda5f"' : 'data-target="#xs-components-links-module-AppModule-62bf2c07dffb82cead56215221735e586d5fd9b5dab0caf8283b91e214e8ce797de3772954db8ea14e6a894a0e26f4272e0792d03001fd3bf71f91a9b4edda5f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-62bf2c07dffb82cead56215221735e586d5fd9b5dab0caf8283b91e214e8ce797de3772954db8ea14e6a894a0e26f4272e0792d03001fd3bf71f91a9b4edda5f"' :
                                            'id="xs-components-links-module-AppModule-62bf2c07dffb82cead56215221735e586d5fd9b5dab0caf8283b91e214e8ce797de3772954db8ea14e6a894a0e26f4272e0792d03001fd3bf71f91a9b4edda5f"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link" >CoreModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CoreModule-504b15ec0558d48761a1f006f3952daccd439e9d663d1f0261ee79c43fa892600cd84beaad0c40f2d35e075934a446ad3349450e5683687b9381c7f3ddf605d0"' : 'data-target="#xs-components-links-module-CoreModule-504b15ec0558d48761a1f006f3952daccd439e9d663d1f0261ee79c43fa892600cd84beaad0c40f2d35e075934a446ad3349450e5683687b9381c7f3ddf605d0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CoreModule-504b15ec0558d48761a1f006f3952daccd439e9d663d1f0261ee79c43fa892600cd84beaad0c40f2d35e075934a446ad3349450e5683687b9381c7f3ddf605d0"' :
                                            'id="xs-components-links-module-CoreModule-504b15ec0558d48761a1f006f3952daccd439e9d663d1f0261ee79c43fa892600cd84beaad0c40f2d35e075934a446ad3349450e5683687b9381c7f3ddf605d0"' }>
                                            <li class="link">
                                                <a href="components/FooterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FooterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HeaderComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NavComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CustomersModule.html" data-type="entity-link" >CustomersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CustomersModule-546aab8322229c5df04fda84868a92b7fb09906bba23ccdc66c4fbbece04f815d0c88f98a70f69e21f097d18d05d8386803f8ef6dc6e4fd8ae48825b59415173"' : 'data-target="#xs-components-links-module-CustomersModule-546aab8322229c5df04fda84868a92b7fb09906bba23ccdc66c4fbbece04f815d0c88f98a70f69e21f097d18d05d8386803f8ef6dc6e4fd8ae48825b59415173"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CustomersModule-546aab8322229c5df04fda84868a92b7fb09906bba23ccdc66c4fbbece04f815d0c88f98a70f69e21f097d18d05d8386803f8ef6dc6e4fd8ae48825b59415173"' :
                                            'id="xs-components-links-module-CustomersModule-546aab8322229c5df04fda84868a92b7fb09906bba23ccdc66c4fbbece04f815d0c88f98a70f69e21f097d18d05d8386803f8ef6dc6e4fd8ae48825b59415173"' }>
                                            <li class="link">
                                                <a href="components/FormCustomerComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormCustomerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageAddCustomersComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageAddCustomersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageEditCustomersComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageEditCustomersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageListCustomersComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageListCustomersComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CustomersRoutingModule.html" data-type="entity-link" >CustomersRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/IconsModule.html" data-type="entity-link" >IconsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-IconsModule-4612e24a17ea6ccc7a1459160097734da27faa9d0b4c3b603093791fe581a1d5378cca7855f5f07bd3787327c67409134572150acce02cbd2cda763264de025b"' : 'data-target="#xs-components-links-module-IconsModule-4612e24a17ea6ccc7a1459160097734da27faa9d0b4c3b603093791fe581a1d5378cca7855f5f07bd3787327c67409134572150acce02cbd2cda763264de025b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-IconsModule-4612e24a17ea6ccc7a1459160097734da27faa9d0b4c3b603093791fe581a1d5378cca7855f5f07bd3787327c67409134572150acce02cbd2cda763264de025b"' :
                                            'id="xs-components-links-module-IconsModule-4612e24a17ea6ccc7a1459160097734da27faa9d0b4c3b603093791fe581a1d5378cca7855f5f07bd3787327c67409134572150acce02cbd2cda763264de025b"' }>
                                            <li class="link">
                                                <a href="components/AddComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AddComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BurgerComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >BurgerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CloseComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CloseComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DeleteComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DeleteComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EditComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EditComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginModule.html" data-type="entity-link" >LoginModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoginModule-d4d933a8f7d3025af21cc4c5ce6613090891e22243d0b8216f11cc0b4e0b5bc2181042e604d55d1a7a3179ce510344c3fe3e82c990ce154600ffe0344fc05c4c"' : 'data-target="#xs-components-links-module-LoginModule-d4d933a8f7d3025af21cc4c5ce6613090891e22243d0b8216f11cc0b4e0b5bc2181042e604d55d1a7a3179ce510344c3fe3e82c990ce154600ffe0344fc05c4c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginModule-d4d933a8f7d3025af21cc4c5ce6613090891e22243d0b8216f11cc0b4e0b5bc2181042e604d55d1a7a3179ce510344c3fe3e82c990ce154600ffe0344fc05c4c"' :
                                            'id="xs-components-links-module-LoginModule-d4d933a8f7d3025af21cc4c5ce6613090891e22243d0b8216f11cc0b4e0b5bc2181042e604d55d1a7a3179ce510344c3fe3e82c990ce154600ffe0344fc05c4c"' }>
                                            <li class="link">
                                                <a href="components/PageForgotPasswordComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageForgotPasswordComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageResetPasswordComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageResetPasswordComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageSignInComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageSignInComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageSignUpComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageSignUpComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginRoutingModule.html" data-type="entity-link" >LoginRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/OrdersModule.html" data-type="entity-link" >OrdersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-OrdersModule-3d829bdd307a374b1a984ed884e5f1f6145f12205de0606bfb8401ced97db4f88a6efc84da1ad1a541923bc2c7ee7c1be824b58ff39c030bf7a5cdb9c49f9c85"' : 'data-target="#xs-components-links-module-OrdersModule-3d829bdd307a374b1a984ed884e5f1f6145f12205de0606bfb8401ced97db4f88a6efc84da1ad1a541923bc2c7ee7c1be824b58ff39c030bf7a5cdb9c49f9c85"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-OrdersModule-3d829bdd307a374b1a984ed884e5f1f6145f12205de0606bfb8401ced97db4f88a6efc84da1ad1a541923bc2c7ee7c1be824b58ff39c030bf7a5cdb9c49f9c85"' :
                                            'id="xs-components-links-module-OrdersModule-3d829bdd307a374b1a984ed884e5f1f6145f12205de0606bfb8401ced97db4f88a6efc84da1ad1a541923bc2c7ee7c1be824b58ff39c030bf7a5cdb9c49f9c85"' }>
                                            <li class="link">
                                                <a href="components/FormOrdersComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormOrdersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageAddOrdersComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageAddOrdersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageEditOrdersComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageEditOrdersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageListOrdersComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageListOrdersComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/OrdersRoutingModule.html" data-type="entity-link" >OrdersRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PageNotFoundModule.html" data-type="entity-link" >PageNotFoundModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PageNotFoundModule-4a18d3361596e88e5c6799ba6e66147931e42a1b3032121d4857ba55ee0ff8b33e1ae3c09bcd6d2f03ed74c43508cdba22e6595df1bfea98ba9d4d22f9cc9293"' : 'data-target="#xs-components-links-module-PageNotFoundModule-4a18d3361596e88e5c6799ba6e66147931e42a1b3032121d4857ba55ee0ff8b33e1ae3c09bcd6d2f03ed74c43508cdba22e6595df1bfea98ba9d4d22f9cc9293"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PageNotFoundModule-4a18d3361596e88e5c6799ba6e66147931e42a1b3032121d4857ba55ee0ff8b33e1ae3c09bcd6d2f03ed74c43508cdba22e6595df1bfea98ba9d4d22f9cc9293"' :
                                            'id="xs-components-links-module-PageNotFoundModule-4a18d3361596e88e5c6799ba6e66147931e42a1b3032121d4857ba55ee0ff8b33e1ae3c09bcd6d2f03ed74c43508cdba22e6595df1bfea98ba9d4d22f9cc9293"' }>
                                            <li class="link">
                                                <a href="components/PageNotFoundComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageNotFoundComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PageNotFoundRoutingModule.html" data-type="entity-link" >PageNotFoundRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ProductsModule.html" data-type="entity-link" >ProductsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ProductsModule-4ad2330ef0974c13c2cc6562c11903f62885f734eaa84efa91ebad11c7aed7018509095e893b389359f8aa815ef1c8b1fbae9f8017083dbf181245571288d6ef"' : 'data-target="#xs-components-links-module-ProductsModule-4ad2330ef0974c13c2cc6562c11903f62885f734eaa84efa91ebad11c7aed7018509095e893b389359f8aa815ef1c8b1fbae9f8017083dbf181245571288d6ef"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ProductsModule-4ad2330ef0974c13c2cc6562c11903f62885f734eaa84efa91ebad11c7aed7018509095e893b389359f8aa815ef1c8b1fbae9f8017083dbf181245571288d6ef"' :
                                            'id="xs-components-links-module-ProductsModule-4ad2330ef0974c13c2cc6562c11903f62885f734eaa84efa91ebad11c7aed7018509095e893b389359f8aa815ef1c8b1fbae9f8017083dbf181245571288d6ef"' }>
                                            <li class="link">
                                                <a href="components/FormProductComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormProductComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageAddProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageAddProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageEditProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageEditProductsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageListProductsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageListProductsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ProductsRoutingModule.html" data-type="entity-link" >ProductsRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link" >SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-83f2bee48b3e42e38d0992aee9cf49c8e5849fed8b7119a34b0de3775a31dabe06c7356c3a2197b6e57f343f7b926af6ba9f54f7bda30642808f7b0962c6e84f"' : 'data-target="#xs-components-links-module-SharedModule-83f2bee48b3e42e38d0992aee9cf49c8e5849fed8b7119a34b0de3775a31dabe06c7356c3a2197b6e57f343f7b926af6ba9f54f7bda30642808f7b0962c6e84f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-83f2bee48b3e42e38d0992aee9cf49c8e5849fed8b7119a34b0de3775a31dabe06c7356c3a2197b6e57f343f7b926af6ba9f54f7bda30642808f7b0962c6e84f"' :
                                            'id="xs-components-links-module-SharedModule-83f2bee48b3e42e38d0992aee9cf49c8e5849fed8b7119a34b0de3775a31dabe06c7356c3a2197b6e57f343f7b926af6ba9f54f7bda30642808f7b0962c6e84f"' }>
                                            <li class="link">
                                                <a href="components/TableComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TableComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TemplatesModule.html" data-type="entity-link" >TemplatesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TemplatesModule-f4822eab91cef8f64fc09d69b70f0bf83ce79b81e49eaf4cea30acaa93fbc13962341d42fa283c04bc100b408e656f20d91f141f87033a3edc28155b7334fb90"' : 'data-target="#xs-components-links-module-TemplatesModule-f4822eab91cef8f64fc09d69b70f0bf83ce79b81e49eaf4cea30acaa93fbc13962341d42fa283c04bc100b408e656f20d91f141f87033a3edc28155b7334fb90"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TemplatesModule-f4822eab91cef8f64fc09d69b70f0bf83ce79b81e49eaf4cea30acaa93fbc13962341d42fa283c04bc100b408e656f20d91f141f87033a3edc28155b7334fb90"' :
                                            'id="xs-components-links-module-TemplatesModule-f4822eab91cef8f64fc09d69b70f0bf83ce79b81e49eaf4cea30acaa93fbc13962341d42fa283c04bc100b408e656f20d91f141f87033a3edc28155b7334fb90"' }>
                                            <li class="link">
                                                <a href="components/TemplateContainerComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >TemplateContainerComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UiModule.html" data-type="entity-link" >UiModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UiModule-eef3307c94b7eaad10200e68428746b2e2917e740e1a3b89af55bd6ba40de95194b982402900d98eb4bc83a674be8f0f5eb6cbf6190a187b5ffbe80dae4ab6dc"' : 'data-target="#xs-components-links-module-UiModule-eef3307c94b7eaad10200e68428746b2e2917e740e1a3b89af55bd6ba40de95194b982402900d98eb4bc83a674be8f0f5eb6cbf6190a187b5ffbe80dae4ab6dc"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UiModule-eef3307c94b7eaad10200e68428746b2e2917e740e1a3b89af55bd6ba40de95194b982402900d98eb4bc83a674be8f0f5eb6cbf6190a187b5ffbe80dae4ab6dc"' :
                                            'id="xs-components-links-module-UiModule-eef3307c94b7eaad10200e68428746b2e2917e740e1a3b89af55bd6ba40de95194b982402900d98eb4bc83a674be8f0f5eb6cbf6190a187b5ffbe80dae4ab6dc"' }>
                                            <li class="link">
                                                <a href="components/LayoutComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LayoutComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersModule.html" data-type="entity-link" >UsersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UsersModule-63eb987f5c5587f56389f92ffd6f94e71a74df784d19c546c28ed0461321f6c0d0e79be5e84a4e2d41ea40ec4a116bc38b4ce24ae5005326fa2723e24a7950de"' : 'data-target="#xs-components-links-module-UsersModule-63eb987f5c5587f56389f92ffd6f94e71a74df784d19c546c28ed0461321f6c0d0e79be5e84a4e2d41ea40ec4a116bc38b4ce24ae5005326fa2723e24a7950de"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UsersModule-63eb987f5c5587f56389f92ffd6f94e71a74df784d19c546c28ed0461321f6c0d0e79be5e84a4e2d41ea40ec4a116bc38b4ce24ae5005326fa2723e24a7950de"' :
                                            'id="xs-components-links-module-UsersModule-63eb987f5c5587f56389f92ffd6f94e71a74df784d19c546c28ed0461321f6c0d0e79be5e84a4e2d41ea40ec4a116bc38b4ce24ae5005326fa2723e24a7950de"' }>
                                            <li class="link">
                                                <a href="components/FormUsersComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormUsersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageAddUsersComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageAddUsersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageEditUsersComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageEditUsersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PageListUsersComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PageListUsersComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersRoutingModule.html" data-type="entity-link" >UsersRoutingModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Customer.html" data-type="entity-link" >Customer</a>
                            </li>
                            <li class="link">
                                <a href="classes/Order.html" data-type="entity-link" >Order</a>
                            </li>
                            <li class="link">
                                <a href="classes/Product.html" data-type="entity-link" >Product</a>
                            </li>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link" >User</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/CustomersService.html" data-type="entity-link" >CustomersService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/OrdersService.html" data-type="entity-link" >OrdersService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ProductsService.html" data-type="entity-link" >ProductsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UsersService.html" data-type="entity-link" >UsersService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/CustomerI.html" data-type="entity-link" >CustomerI</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/OrderI.html" data-type="entity-link" >OrderI</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ProductI.html" data-type="entity-link" >ProductI</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/UserI.html" data-type="entity-link" >UserI</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});