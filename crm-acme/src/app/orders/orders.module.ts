import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { PageListOrdersComponent } from './pages/page-list-orders/page-list-orders.component';
import { PageAddOrdersComponent } from './pages/page-add-orders/page-add-orders.component';
import { PageEditOrdersComponent } from './pages/page-edit-orders/page-edit-orders.component';
import {SharedModule} from "../shared/shared.module";
import {IconsModule} from "../icons/icons.module";
import { FormOrdersComponent } from './components/form-orders/form-orders.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TemplatesModule} from "../templates/templates.module";
import {Ng2SearchPipeModule} from "ng2-search-filter";


@NgModule({
  declarations: [
    PageListOrdersComponent,
    PageAddOrdersComponent,
    PageEditOrdersComponent,
    FormOrdersComponent
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    SharedModule,
    IconsModule,
    ReactiveFormsModule,
    TemplatesModule,
    FormsModule,
    Ng2SearchPipeModule
  ],
  exports: [
    PageListOrdersComponent,
    PageAddOrdersComponent,
    PageEditOrdersComponent
  ]
})
export class OrdersModule { }
