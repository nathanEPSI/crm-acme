import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Order} from "../../../core/models/order";
import {OrdersService} from "../../orders.service";

@Component({
  selector: 'app-page-list-orders',
  templateUrl: './page-list-orders.component.html',
  styleUrls: ['./page-list-orders.component.scss']
})
export class PageListOrdersComponent implements OnInit {

  public searchTerm! : string;

  public ordersHeaders = [
    'Id',
    'Id clients',
    'Total HT',
    'Date de commande',
    'Statut de livraison',
    'Statut du paiement'
  ];

  public $orders!: Observable<Order[]>;

  constructor(private ordersService: OrdersService) { }

  ngOnInit(): void {
    this.getOrders();
  }

  delete(id: number): void {
    this.ordersService.deleteItemById(id).subscribe(() => this.getOrders());
  }

  deleteConfirm(id: number) {
    if (confirm("Veuillez confirmer la suppression?")) {
      this.delete(id);
    }

  }

  getOrders(): void {
    this.$orders = this.ordersService.getCollection();
  }

}
