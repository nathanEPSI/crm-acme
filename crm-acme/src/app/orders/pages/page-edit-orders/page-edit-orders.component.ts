import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Order} from "../../../core/models/order";
import {OrdersService} from "../../orders.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-page-edit-orders',
  templateUrl: './page-edit-orders.component.html',
  styleUrls: ['./page-edit-orders.component.scss']
})
export class PageEditOrdersComponent implements OnInit {

  public $order!: Observable<Order>;

  constructor(
    private ordersService: OrdersService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.$order = this.ordersService.getItemById(id);
  }

  public editOrder(order: Order): void {
    this.ordersService.updateItem(order).subscribe(
      result => this.router.navigate(['/home/orders'])
    );
  }

}
