import { Component, OnInit } from '@angular/core';
import {Order} from "../../../core/models/order";
import {OrdersService} from "../../orders.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-page-add-orders',
  templateUrl: './page-add-orders.component.html',
  styleUrls: ['./page-add-orders.component.scss']
})
export class PageAddOrdersComponent implements OnInit {

  public newOrder: Order = new Order();

  constructor(private ordersService: OrdersService,
              private router: Router) { }

  ngOnInit(): void {
  }

  public addOrder(order: Order): void {
    this.ordersService.addItem(order).subscribe(
      result => this.router.navigate(['/home/orders'])
    );
  }

}
