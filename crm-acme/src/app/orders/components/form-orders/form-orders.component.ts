import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Order} from "../../../core/models/order";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-form-orders',
  templateUrl: './form-orders.component.html',
  styleUrls: ['./form-orders.component.scss']
})
export class FormOrdersComponent implements OnInit {

  @Input()
  public initOrder!: Order;

  @Output()
  public submittedOrder = new EventEmitter<Order>();

  public orderForm!: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.orderForm = this.fb.group({
      id: [this.initOrder.id],
      customerId: [this.initOrder.customerId, Validators.required],
      totalExclTax: [this.initOrder.totalExclTax, Validators.required],
      ordersDate: [this.initOrder.ordersDate],
      deliveryStatus: [this.initOrder.deliveryStatus],
      paymentStatus: [this.initOrder.paymentStatus]
    })
  }

  public onSubmit(): void {
    if (this.orderForm.valid) {
      this.submittedOrder.emit(this.orderForm.value);
    } else {
      this.orderForm.markAllAsTouched();
    }
  }

}
