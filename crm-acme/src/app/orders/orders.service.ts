import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Order} from "../core/models/order";

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  private url: string = '/api/v1/orders';

  constructor(private httpClient: HttpClient) { }

  getItemById(id: number): Observable<Order> {
    return this.httpClient.get<Order>(this.url + '/' + id);
  }

  getCollection(): Observable<Order[]> {
    return this.httpClient.get<Order[]>(this.url);
  }

  deleteItemById(id: number): Observable<Order> {
    return this.httpClient.delete<Order>(this.url + '/' + id);
  }

  updateItem(order: Order): Observable<Order> {
    return this.httpClient.put<Order>(this.url + '/' + order.id, order);
  }

  addItem(order: Order): Observable<Order> {
    return this.httpClient.post<Order>(this.url, order);
  }
}
