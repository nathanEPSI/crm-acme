import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UiModule} from '../ui/ui.module';
import {IconsModule} from '../icons/icons.module';
import {TemplatesModule} from '../templates/templates.module';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {NavComponent} from './components/nav/nav.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from "@angular/router";
import {HomeComponent} from "./pages/home/home.component";

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    NavComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    IconsModule,
    TemplatesModule,
    UiModule
  ],
  exports: []
})
export class CoreModule {
}
