export interface CustomerI {
    id: number;
    firstname: string;
    lastname: string;
    company: string;
    email: string;
    phone: string;
    active: boolean;
}