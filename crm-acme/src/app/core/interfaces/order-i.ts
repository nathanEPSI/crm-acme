export interface OrderI {
  id: number;
  customerId: number;
  totalExclTax: number;
  ordersDate: Date;
  deliveryStatus: boolean;
  paymentStatus: boolean;
}
