export interface ProductI {
  id: number;
  name: string;
  priceExclTax: number;
  taxRate: number;
}
