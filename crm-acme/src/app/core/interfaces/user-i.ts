export interface UserI {
  id: number;
  username: string;
  password: string;
  email: string;
  type: number;
}
