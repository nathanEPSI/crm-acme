import {UserI} from "../interfaces/user-i";


export class User implements UserI {

  id = 0;
  username = '';
  password = '';
  email = '';
  type = 0;


  constructor(obj?: Partial<User>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }

}

