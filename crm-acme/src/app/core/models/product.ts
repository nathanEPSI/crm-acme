import {ProductI} from "../interfaces/product-i";

export class Product implements ProductI{

  id = 0;
  name = '';
  priceExclTax = 0;
  taxRate = 0;


  constructor(obj? : Partial<Product>) {
    if (obj){
      Object.assign(this, obj);
    }

  }

}

