import { OrderI } from "../interfaces/order-i";

export class Order implements OrderI {

  id: number = 0;
  customerId: number = 0;
  totalExclTax: number = 0;
  ordersDate: Date = new Date();
  deliveryStatus: boolean = false;
  paymentStatus: boolean = true;


  constructor(obj? : Partial<Order>) {
    if (obj) {
      Object.assign(this, obj);
    }
  }

}
