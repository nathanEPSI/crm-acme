import { CustomerI } from "../interfaces/customer-i";

export class Customer implements CustomerI {
    id = 0;
    firstname = '';
    lastname = '';
    company = '';
    email = '';
    phone = '';
    active = true;    

    constructor(obj? : Partial<Customer>) {
        if (obj) {
            Object.assign(this, obj);
        }
    }
}