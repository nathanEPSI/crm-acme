import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private url: string = '/api/v1/logout';

  constructor(private httpClient: HttpClient,
              private router: Router) { }

  ngOnInit(): void {
  }

  public logout(): void {
    this.httpClient.get(this.url).subscribe(
      result => this.router.navigate(['login/signin'])
    );
  }

}
