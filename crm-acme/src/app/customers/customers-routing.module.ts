import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageListCustomersComponent} from './pages/page-list-customers/page-list-customers.component';
import {PageAddCustomersComponent} from './pages/page-add-customers/page-add-customers.component';
import {PageEditCustomersComponent} from './pages/page-edit-customers/page-edit-customers.component';

const routes: Routes = [
  {path: '', component: PageListCustomersComponent},
  {path: 'add', component: PageAddCustomersComponent},
  {path: 'edit/:id', component: PageEditCustomersComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule {
}
