import {Component, OnInit} from '@angular/core';
import {delay, Observable} from 'rxjs';
import {Customer} from 'src/app/core/models/customer';
import {CustomersService} from '../../customers.service';

@Component({
  selector: 'app-page-list-customers',
  templateUrl: './page-list-customers.component.html',
  styleUrls: ['./page-list-customers.component.scss']
})
export class PageListCustomersComponent implements OnInit {

  public searchTerm! : string;

  public customersHeaders = [
    'Id',
    'Prénom du Client',
    'Nom du Client',
    'Société',
    'E-mail',
    'Numero de télephone',
    'Statut'
  ];

  public $customers!: Observable<Customer[]>;

  constructor(private customersService: CustomersService) {
  }

   ngOnInit(): void {
    this.getCustomers();
  }

  delete(customer: number): void {
    this.customersService.deleteItemById(customer).subscribe(() => this.getCustomers());
  }


  deleteConfirm(id: number) {
    if (confirm("Veuillez confirmer la suppression?")) {
      this.delete(id);
    }
  }


  getCustomers(): void {
    this.$customers = this.customersService.getCollection();
  }
}
