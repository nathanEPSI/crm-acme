import { Component, OnInit } from '@angular/core';
import {Customer} from "../../../core/models/customer";
import {CustomersService} from "../../customers.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-page-add-customers',
  templateUrl: './page-add-customers.component.html',
  styleUrls: ['./page-add-customers.component.scss']
})
export class PageAddCustomersComponent implements OnInit {
  public newCustomer = new Customer();

  constructor(private customersService: CustomersService,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  public addCustomer(customer: Customer): void {
    console.log(customer);
    this.customersService.addItem(customer).subscribe(
      res => this.router.navigate(['/home/customers'])
    );
  }
}
