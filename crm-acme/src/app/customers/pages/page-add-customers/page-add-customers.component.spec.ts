import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAddCustomersComponent } from './page-add-customers.component';

describe('PageAddCustomersComponent', () => {
  let component: PageAddCustomersComponent;
  let fixture: ComponentFixture<PageAddCustomersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageAddCustomersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAddCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
