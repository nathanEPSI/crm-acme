import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageEditCustomersComponent } from './page-edit-customers.component';

describe('PageEditCustomersComponent', () => {
  let component: PageEditCustomersComponent;
  let fixture: ComponentFixture<PageEditCustomersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageEditCustomersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageEditCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
