import { Component, OnInit } from '@angular/core';
import {Customer} from "../../../core/models/customer";
import {Observable} from "rxjs";
import {CustomersService} from "../../customers.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-page-edit-customers',
  templateUrl: './page-edit-customers.component.html',
  styleUrls: ['./page-edit-customers.component.scss']
})
export class PageEditCustomersComponent implements OnInit {

  public customer$!: Observable<Customer>;

  constructor(
    private customerService: CustomersService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.customer$ = this.customerService.getItemById(id);
  }

  public updateCustomer(item: Customer): void {
    this.customerService.updateItem(item).subscribe((res) => {
      this.router.navigate(['/home/customers']);
    });

  }
}
