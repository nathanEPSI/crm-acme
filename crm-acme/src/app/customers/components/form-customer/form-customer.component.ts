import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Customer} from "../../../core/models/customer";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-form-customer',
  templateUrl: './form-customer.component.html',
  styleUrls: ['./form-customer.component.scss']
})


export class FormCustomerComponent implements OnInit {

  @Input()
  public initCustomer!: Customer;

  @Output()
  public submittedCustomer = new EventEmitter<Customer>();

  public customerForm!: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.customerForm = this.fb.group({
      id: [this.initCustomer.id],
      firstname: [this.initCustomer.firstname,[Validators.required]],
      lastname: [this.initCustomer.lastname,[Validators.required]],
      company: [this.initCustomer.company],
      email: [this.initCustomer.email,[Validators.required]],
      phone: [this.initCustomer.phone],
      active: [this.initCustomer.active,[Validators.required]]
    });
  }

  public onSubmit(): void {
    console.log(this.customerForm);
    if (this.customerForm.valid) {
      console.log('tsest');
      this.submittedCustomer.emit(this.customerForm.value);
    } else {
      this.customerForm.markAllAsTouched();
    }
  }

}
