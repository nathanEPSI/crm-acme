import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomersRoutingModule } from './customers-routing.module';
import { PageListCustomersComponent } from './pages/page-list-customers/page-list-customers.component';
import { PageAddCustomersComponent } from './pages/page-add-customers/page-add-customers.component';
import { PageEditCustomersComponent } from './pages/page-edit-customers/page-edit-customers.component';
import {SharedModule} from "../shared/shared.module";
import {IconsModule} from "../icons/icons.module";
import {FormCustomerComponent} from "./components/form-customer/form-customer.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TemplatesModule} from "../templates/templates.module";
import {Ng2SearchPipeModule} from "ng2-search-filter";


@NgModule({
    declarations: [
        PageListCustomersComponent,
        PageAddCustomersComponent,
        PageEditCustomersComponent,
        FormCustomerComponent
    ],
  imports: [
    CommonModule,
    CustomersRoutingModule,
    SharedModule,
    IconsModule,
    ReactiveFormsModule,
    TemplatesModule,
    FormsModule,
    Ng2SearchPipeModule
  ],
  exports: [
    PageListCustomersComponent,
    PageAddCustomersComponent,
    PageEditCustomersComponent
  ]
})
export class CustomersModule { }
