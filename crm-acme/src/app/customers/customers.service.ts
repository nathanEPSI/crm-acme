import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer } from '../core/models/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {
  private url: string = '/api/v1/customers';

  constructor(private httpClient: HttpClient) { }

  getItemById(id: number): Observable<Customer> {
    return this.httpClient.get<Customer>(this.url + '/' + id);
  }

  getCollection(): Observable<Customer[]> {
    return this.httpClient.get<Customer[]>(this.url);
  }

  deleteItemById(id: number): Observable<Customer> {
    return this.httpClient.delete<Customer>(this.url + '/' + id);
  }

  updateItem(customer: Customer): Observable<Customer> {
    return this.httpClient.put<Customer>(this.url + '/' + customer.id, customer);
  }

  addItem(customer: Customer): Observable<Customer> {
    return this.httpClient.post<Customer>(this.url, customer);
  }
}
