import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginService} from "../../login.service";
import {Router} from "@angular/router";
import {User} from "../../../core/models/user";
import {UsersService} from "../../../users/users.service";
import {switchAll, switchMap} from "rxjs";

@Component({
  selector: 'app-page-sign-up',
  templateUrl: './page-sign-up.component.html',
  styleUrls: ['./page-sign-up.component.scss']
})
export class PageSignUpComponent implements OnInit {

  public newUser: User = new User();

  constructor(private usersService: UsersService,
              private loginService: LoginService,
              private router: Router) { }

  ngOnInit(): void {
  }

  public addUser(user: User): void {
    this.usersService.addItem(user)
      .pipe(
        switchMap(userCreate => this.loginService.authenticate(user.username, user.password))
      )
      .subscribe(
      result => this.router.navigate(['/home/orders'])
    );
  }

}
