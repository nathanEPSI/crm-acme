import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../../../core/models/user";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginService} from "../../login.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-page-sign-in',
  templateUrl: './page-sign-in.component.html',
  styleUrls: ['./page-sign-in.component.scss']
})
export class PageSignInComponent implements OnInit {

  public userForm!: FormGroup;

  constructor(private fb: FormBuilder,
              private loginService: LoginService,
              private router: Router) { }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    console.log(this.userForm);
  }

  public authenticate(): void {
    this.loginService.authenticate(this.userForm.get('username')?.value,
      this.userForm.get('password')?.value).subscribe(
      result => this.router.navigate(['/home/orders'])
    );
  }

}
