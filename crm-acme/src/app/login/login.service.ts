import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {BehaviorSubject, map, Observable} from "rxjs";
import {User} from "../core/models/user";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private connectedUser$ = new BehaviorSubject<any>(null);

  private url: string = '/api/v1/users/login';

  constructor(private httpClient: HttpClient) { }

  public getConnectedUser$(): Observable<any> {
    return this.connectedUser$.asObservable();
  }

  public isAdmin$(): Observable<boolean> {
    return this.connectedUser$
      .pipe(
        map(user => user !== null && user?.type === 1)
      );
  }

  public authenticate(username: string, password: string): Observable<User> {
    const headers = new HttpHeaders({
      authorization: 'Basic ' + btoa(username + ':' + password)
    });


    const params = new HttpParams()
      .set('username', username)
      .set('password', password);

    return this.httpClient.get<User>(this.url, {headers, params}).pipe(
    map(data => {
      this.connectedUser$.next(data);
      localStorage.setItem('myUser', JSON.stringify(data));
      return new User(data);
    })
    );
  }


}
