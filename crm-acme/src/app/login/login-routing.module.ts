import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageResetPasswordComponent } from './pages/page-reset-password/page-reset-password.component';
import { PageSignInComponent } from './pages/page-sign-in/page-sign-in.component';
import { PageSignUpComponent } from './pages/page-sign-up/page-sign-up.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'signin'},
  {path: 'signin', component: PageSignInComponent},
  {path: 'signup', component: PageSignUpComponent},
  {path: 'reset', component: PageResetPasswordComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
