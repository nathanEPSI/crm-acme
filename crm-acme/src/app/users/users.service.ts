import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../core/models/user";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private url: string = '/api/v1/users';

  constructor(private httpClient: HttpClient) { }

  getItemById(id: number): Observable<User> {
    return this.httpClient.get<User>(this.url + '/' + id);
  }

  getCollection(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.url);
  }

  deleteItemById(id: number): Observable<User> {
    return this.httpClient.delete<User>(this.url + '/' + id);
  }

  updateItem(user: User): Observable<User> {
    return this.httpClient.put<User>(this.url + '/' + user.id, user);
  }

  addItem(user: User): Observable<User> {
    return this.httpClient.post<User>(this.url, user);
  }
}
