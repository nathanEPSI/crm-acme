import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { PageListUsersComponent } from './pages/page-list-users/page-list-users.component';
import { PageAddUsersComponent } from './pages/page-add-users/page-add-users.component';
import { PageEditUsersComponent } from './pages/page-edit-users/page-edit-users.component';
import {SharedModule} from "../shared/shared.module";
import {IconsModule} from "../icons/icons.module";
import { FormUsersComponent } from './components/form-users/form-users.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TemplatesModule} from "../templates/templates.module";
import {Ng2SearchPipeModule} from "ng2-search-filter";


@NgModule({
  declarations: [
    PageListUsersComponent,
    PageAddUsersComponent,
    PageEditUsersComponent,
    FormUsersComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    SharedModule,
    IconsModule,
    ReactiveFormsModule,
    TemplatesModule,
    FormsModule,
    Ng2SearchPipeModule
  ],
  exports: [
    PageListUsersComponent,
    PageAddUsersComponent,
    PageEditUsersComponent
  ]
})
export class UsersModule { }
