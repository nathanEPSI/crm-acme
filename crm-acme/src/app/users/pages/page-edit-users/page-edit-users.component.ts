import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {User} from "../../../core/models/user";
import {UsersService} from "../../users.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-page-edit-users',
  templateUrl: './page-edit-users.component.html',
  styleUrls: ['./page-edit-users.component.scss']
})
export class PageEditUsersComponent implements OnInit {

  public $user!: Observable<User>;

  constructor(
    private usersService: UsersService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.$user = this.usersService.getItemById(id);
  }

  public editUser(user: User): void {
    this.usersService.updateItem(user).subscribe(
      result => this.router.navigate(['/home/users'])
    );
  }

}
