import { Component, OnInit } from '@angular/core';
import {Order} from "../../../core/models/order";
import {User} from "../../../core/models/user";
import {UsersService} from "../../users.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-page-add-users',
  templateUrl: './page-add-users.component.html',
  styleUrls: ['./page-add-users.component.scss']
})
export class PageAddUsersComponent implements OnInit {

  public newUser: User = new User();

  constructor(private usersService: UsersService,
              private router: Router) { }

  ngOnInit(): void {
  }

  public addUser(user: User): void {
    this.usersService.addItem(user).subscribe(
      result => this.router.navigate(['/home/users'])
    );
  }

}
