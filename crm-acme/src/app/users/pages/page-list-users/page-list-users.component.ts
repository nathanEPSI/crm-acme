import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {User} from "../../../core/models/user";
import {UsersService} from "../../users.service";

@Component({
  selector: 'app-page-list-users',
  templateUrl: './page-list-users.component.html',
  styleUrls: ['./page-list-users.component.scss']
})
export class PageListUsersComponent implements OnInit {

  public searchTerm! : string;

  public usersHeaders = [
    'Id',
    'Nom utilisateur',
    'E-mail',
    'Type',
    'Actions'
  ];

  public $users!: Observable<User[]>;

  constructor(private userService: UsersService) { }

  ngOnInit(): void {
    this.getUsers();
  }
  delete(id: number): void {
    this.userService.deleteItemById(id).subscribe(() => this.getUsers());
  }

  deleteConfirm(id: number) {
    if (confirm("Veuillez confirmer la suppression?")) {
      this.delete(id);
    }
  }

  getUsers(): void {
    this.$users = this.userService.getCollection();
  }

}
