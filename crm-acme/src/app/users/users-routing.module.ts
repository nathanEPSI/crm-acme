import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageAddUsersComponent } from './pages/page-add-users/page-add-users.component';
import { PageEditUsersComponent } from './pages/page-edit-users/page-edit-users.component';
import { PageListUsersComponent } from './pages/page-list-users/page-list-users.component';

const routes: Routes = [
  {path: '', component: PageListUsersComponent},
  {path: 'add', component: PageAddUsersComponent},
  {path: 'edit/:id', component: PageEditUsersComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
