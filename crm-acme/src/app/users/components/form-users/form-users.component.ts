import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../../../core/models/user";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-form-users',
  templateUrl: './form-users.component.html',
  styleUrls: ['./form-users.component.scss']
})
export class FormUsersComponent implements OnInit {

  @Input()
  public initUser!: User;

  @Output()
  public submittedUser =  new EventEmitter<User>();

  public userForm!: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      id: [this.initUser.id],
      username: [this.initUser.username, Validators.required],
      password: [this.initUser.password, Validators.required],
      email: [this.initUser.email, Validators.required],
      type: [this.initUser.type]
    })
  }

  public onSubmit(): void {
    if (this.userForm.valid) {
      this.submittedUser.emit(this.userForm.value);
    } else {
      this.userForm.markAllAsTouched();
    }
  }

}
