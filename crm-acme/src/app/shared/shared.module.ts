import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './components/table/table.component';
import {IconsModule} from "../icons/icons.module";



@NgModule({
    declarations: [
        TableComponent
    ],
    exports: [
        TableComponent
    ],
  imports: [
    CommonModule,
    IconsModule
  ]
})
export class SharedModule { }
