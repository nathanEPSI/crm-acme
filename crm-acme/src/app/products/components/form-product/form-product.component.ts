import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from "../../../core/models/product";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-form-product',
  templateUrl: './form-product.component.html',
  styleUrls: ['./form-product.component.scss']
})
export class FormProductComponent implements OnInit {

  @Input()
  public initProduct!: Product;

  @Output()
  public submittedProduct = new EventEmitter<Product>();

  public productForm!: FormGroup

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.productForm = this.fb.group(
      {
        id:[this.initProduct.id],
        name: [this.initProduct.name, Validators.required],
        priceExclTax: [this.initProduct.priceExclTax, Validators.required],
        taxRate: [this.initProduct.taxRate, Validators.required],
      }
    )
  }

  public onSubmit(): void {
    if(this.productForm.valid) {
      this.submittedProduct.emit(this.productForm.value);
    } else {
      this.productForm.markAllAsTouched();
    }
  }


}
