import { Component, OnInit } from '@angular/core';
import {Product} from "../../../core/models/product";
import {Observable} from "rxjs";
import {ProductsService} from "../../products.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-page-edit-products',
  templateUrl: './page-edit-products.component.html',
  styleUrls: ['./page-edit-products.component.scss']
})
export class PageEditProductsComponent implements OnInit {


  public item$!: Observable<Product>;

  constructor(private productsService: ProductsService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.item$ = this.productsService.getItemById(id);
  }


  public updateProduct(item: Product): void {
    this.productsService.updateItem(item).subscribe((res) => {
      this.router.navigate(['/home/products']);
    });
  }


}
