import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageEditProductsComponent } from './page-edit-products.component';

describe('PageEditProductsComponent', () => {
  let component: PageEditProductsComponent;
  let fixture: ComponentFixture<PageEditProductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageEditProductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageEditProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
