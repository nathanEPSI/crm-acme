import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAddProductsComponent } from './page-add-products.component';

describe('PageAddProductsComponent', () => {
  let component: PageAddProductsComponent;
  let fixture: ComponentFixture<PageAddProductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageAddProductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAddProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
