import { Component, OnInit } from '@angular/core';
import {Order} from "../../../core/models/order";
import {Product} from "../../../core/models/product";
import {ProductsService} from "../../products.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-page-add-products',
  templateUrl: './page-add-products.component.html',
  styleUrls: ['./page-add-products.component.scss']
})
export class PageAddProductsComponent implements OnInit {

  public newProduct = new Product()
  constructor(private productService: ProductsService, private router: Router) { }

  ngOnInit(): void {
  }


  public addProduct(product: Product): void {
    this.productService.addItem(product).subscribe(
      res => this.router.navigate(['/home/products'])
    );
  }

}
