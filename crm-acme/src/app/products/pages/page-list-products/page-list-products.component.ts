import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {ProductsService} from "../../products.service";
import {Product} from "../../../core/models/product";

@Component({
  selector: 'app-page-list-products',
  templateUrl: './page-list-products.component.html',
  styleUrls: ['./page-list-products.component.scss']
})
export class PageListProductsComponent implements OnInit {

  public searchTerm! : string;

  public productsHeaders = [
    'Id',
    'Désignation du produit',
    'Prix HT',
    'Tva'
  ];

  public $products!: Observable<Product[]>;

  constructor(private productsService: ProductsService) {
  }

  ngOnInit(): void {
    this.getProducts();
  }

  delete(product: number): void {
    this.productsService.deleteItemById(product).subscribe(() => this.getProducts());
  }

  deleteConfirm(id: number) {
    if (confirm("Veuillez confirmer la suppression?")) {
      this.delete(id);
    }
  }

  getProducts(): void {
    this.$products = this.productsService.getCollection();
  }

}
