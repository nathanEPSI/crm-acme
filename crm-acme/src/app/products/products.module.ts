import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { PageListProductsComponent } from './pages/page-list-products/page-list-products.component';
import { PageAddProductsComponent } from './pages/page-add-products/page-add-products.component';
import { PageEditProductsComponent } from './pages/page-edit-products/page-edit-products.component';
import {IconsModule} from "../icons/icons.module";
import {SharedModule} from "../shared/shared.module";
import { FormProductComponent } from './components/form-product/form-product.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {TemplateContainerComponent} from "../templates/components/template-container/template-container.component";
import {TemplatesModule} from "../templates/templates.module";
import {Ng2SearchPipeModule} from "ng2-search-filter";


@NgModule({
  declarations: [
    PageListProductsComponent,
    PageAddProductsComponent,
    PageEditProductsComponent,
    FormProductComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    IconsModule,
    SharedModule,
    ReactiveFormsModule,
    TemplatesModule,
    FormsModule,
    Ng2SearchPipeModule
  ],
  exports: [
    PageListProductsComponent,
    PageAddProductsComponent,
    PageEditProductsComponent
  ]
})
export class ProductsModule { }
