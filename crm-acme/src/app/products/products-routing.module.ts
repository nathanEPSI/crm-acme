import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageAddProductsComponent } from './pages/page-add-products/page-add-products.component';
import { PageEditProductsComponent } from './pages/page-edit-products/page-edit-products.component';
import { PageListProductsComponent } from './pages/page-list-products/page-list-products.component';

const routes: Routes = [
  {path: '', component: PageListProductsComponent},
  {path: 'add', component: PageAddProductsComponent},
  {path: 'edit/:id', component: PageEditProductsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
