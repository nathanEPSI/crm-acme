import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Product} from "../core/models/product";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private url: string = '/api/v1/products';

  constructor(private httpClient: HttpClient) { }

  getItemById(id: number): Observable<Product> {
    return this.httpClient.get<Product>(this.url + '/' + id);
  }

  getCollection(): Observable<Product[]> {
    return this.httpClient.get<Product[]>(this.url);
  }

  deleteItemById(id: number): Observable<Product> {
    return this.httpClient.delete<Product>(this.url + '/' + id);
  }

  updateItem(product: Product): Observable<Product> {
    return this.httpClient.put<Product>(this.url + '/' + product.id, product);
  }

  addItem(product:Product): Observable<Product> {
    return this.httpClient.post<Product>(this.url, product);
  }
}
