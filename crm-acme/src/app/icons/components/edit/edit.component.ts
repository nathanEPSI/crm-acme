import { Component, OnInit } from '@angular/core';
import {IconDefinition} from "@fortawesome/free-regular-svg-icons";
import {faEdit} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  public myIcon : IconDefinition = faEdit;

  constructor() { }

  ngOnInit(): void {
  }

}
