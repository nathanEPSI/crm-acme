import { Component, OnInit } from '@angular/core';
import {IconDefinition} from "@fortawesome/free-regular-svg-icons";
import {faTimes} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-close',
  template: `<fa-icon [icon]="myIcon"></fa-icon>`
})
export class CloseComponent implements OnInit {

  public myIcon : IconDefinition = faTimes;

  constructor() { }

  ngOnInit(): void {
  }

}
