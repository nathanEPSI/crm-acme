import { Component, OnInit } from '@angular/core';
import {IconDefinition} from "@fortawesome/free-regular-svg-icons";
import {faBars} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-burger',
  templateUrl: './burger.component.html',
  styleUrls: ['./burger.component.scss'],
  styles: [
  ]
})
export class BurgerComponent implements OnInit {

  public myIcon : IconDefinition = faBars;

  constructor() { }

  ngOnInit(): void {
  }

}
