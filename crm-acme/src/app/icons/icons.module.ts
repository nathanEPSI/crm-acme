import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeleteComponent } from './components/delete/delete.component';
import { EditComponent } from './components/edit/edit.component';
import { AddComponent } from './components/add/add.component';
import { CloseComponent } from './components/close/close.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import { BurgerComponent } from './components/burger/burger.component';



@NgModule({
  declarations: [
    DeleteComponent,
    EditComponent,
    AddComponent,
    CloseComponent,
    BurgerComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    CloseComponent,
    DeleteComponent,
    EditComponent,
    AddComponent,
    BurgerComponent
  ],
})
export class IconsModule { }
