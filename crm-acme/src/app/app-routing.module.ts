import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./core/pages/home/home.component";
import {AuthGuard} from "./shared/guards/auth.guard";
import {AdminGuard} from "./shared/guards/admin.guard";

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'login'},
  {
    path: 'login', loadChildren:
        () => import('./login/login.module')
            .then( (module) => module.LoginModule )
  },

  {
    path: 'home',
    canActivate: [AuthGuard],
    component: HomeComponent,
    children: [
      {path: 'orders', loadChildren:
          () => import('./orders/orders.module')
            .then( (module) => module.OrdersModule )
      },

      {path: 'customers', loadChildren:
          () => import('./customers/customers.module')
            .then( (module) => module.CustomersModule )
      },

      {path: 'users',
        canActivate: [AuthGuard, AdminGuard],
        loadChildren:
          () => import('./users/users.module')
            .then( (module) => module.UsersModule )
      },

      {path: 'products', loadChildren:
          () => import('./products/products.module')
            .then( (module) => module.ProductsModule )
      },
    ]
  },


  {path: '**', loadChildren: () =>
        import('./page-not-found/page-not-found.module')
        .then((m) => m.PageNotFoundModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
